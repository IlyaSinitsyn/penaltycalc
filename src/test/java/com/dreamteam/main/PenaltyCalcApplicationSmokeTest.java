package com.dreamteam.main;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

class PenaltyCalcApplicationSmokeTest {

    @Test
    void main() {
        assertDoesNotThrow(() -> PenaltyCalcApplication.main(new String[0]));
    }
}