package com.dreamteam.impl.config;

import com.dreamteam.impl.controllers.DebtController;
import com.dreamteam.impl.logic.DebtHandler;
import com.dreamteam.impl.logic.DebtService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(ThymeleafConfig.class)
public class PenaltyCalcConfiguration {

    @Bean
    DebtHandler debtHandler() {
        return new DebtHandler();
    }

    @Bean
    DebtService debtService(DebtHandler debtHandler) {
        return new DebtService(debtHandler);
    }

    @Bean
    DebtController debtController(DebtService debtService) {
        return new DebtController(debtService);
    }
}
