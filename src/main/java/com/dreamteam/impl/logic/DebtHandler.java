package com.dreamteam.impl.logic;

import org.springframework.stereotype.Component;

@Component
public class DebtHandler {
    public static final Double KEY_RATE = 7.6;

    public Double calcDebt(Double debtSum, int daysCount) {
        //get keyrate from API CBR
        return debtSum * daysCount * 1 / 300 * KEY_RATE / 100;
    }
}
