package com.dreamteam.impl.logic;

import org.springframework.stereotype.Service;

@Service
public class DebtService {
    private final DebtHandler debtHandler;

    public DebtService(DebtHandler debtHandler) {
        this.debtHandler = debtHandler;
    }

    public String getDebt(Double debtSum, int daysCount) {
        return String.format("%.2f", debtHandler.calcDebt(debtSum, daysCount));
    }
}
