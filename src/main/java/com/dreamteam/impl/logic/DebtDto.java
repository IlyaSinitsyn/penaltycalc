package com.dreamteam.impl.logic;/**

/**
 Created by Gilmutdinov Rishat on 26.06.2023
 */
public class DebtDto {
    private int daysCount;
    private Double debtSum;

    public int getDaysCount() {
        return daysCount;
    }

    public void setDaysCount(int daysCount) {
        this.daysCount = daysCount;
    }

    public Double getDebtSum() {
        return debtSum;
    }

    public void setDebtSum(Double debtSum) {
        this.debtSum = debtSum;
    }
}
