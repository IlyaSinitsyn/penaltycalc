package com.dreamteam.impl.controllers;

import com.dreamteam.impl.logic.DebtDto;
import com.dreamteam.impl.logic.DebtService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class DebtController {
    private final DebtService debtService;

    public DebtController(DebtService debtService) {
        this.debtService = debtService;
    }

    @GetMapping("/*")
    public String getIndex(Model model) {
        model.addAttribute("debtDto", new DebtDto());
        return "index";
    }

    @PostMapping("/index")
    public String submitForm(@ModelAttribute("debtDto") DebtDto debtDto, Model model) {
        model.addAttribute("calcResult", debtService.getDebt(debtDto.getDebtSum(), debtDto.getDaysCount()));
        return "index";
    }
}
