package com.dreamteam.main;

import com.dreamteam.impl.config.PenaltyCalcConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(PenaltyCalcConfiguration.class)
public class PenaltyCalcApplication {
    public static void main(String[] args) {
        SpringApplication.run(PenaltyCalcApplication.class, args);
    }
}
